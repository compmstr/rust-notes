
fn main(){
    //Have to return owned string
    fn fbuzz(num: int) -> ~str {
        let mut s = ~"";
        //.push_str, not +=
        if(num % 3 == 0){s.push_str("fizz");}
        if(num % 5 == 0){s.push_str("buzz");}
        //Note: <string literal> == <owned string> works, but not vice-versa
        if("" == s){s = num.to_str();}
        s
    }
    for num in range(0,100){
        println(fbuzz(num));
    }
}
