//These are to suppress warnings about dead code and unused variables
//  since this file is chock full of them, as examples
#[allow(dead_code)];
#[allow(dead_assignment)];
#[allow(unused_imports)];
#[allow(unused_variable)];
#[allow(unused_mut)];

#[feature(managed_boxes)];
//Not quite stable syntax yet, so feature locked
#[feature(macro_rules)];

//Extra has been deprecated
extern crate collections;
extern crate sync;
extern crate rand;
use std::f64;
use std::f64::consts::PI;
use std::num::atan;
use std::num::sqrt;
use std::mem::size_of;
use std::io::println;
//Should learn how to use these, as managed boxes are deprecated
use std::gc::Gc;
use std::rc::Rc;
use collections::hashmap::HashMap;
//This will be just vec, later
use std::vec::Vec;

fn simple_loop(){
    loop {
        //Tricky calculation
        /*
        if universe::recalibrate() {
            return;
        }
        return;
        */
    }
}

fn let_test(){
    //let keyword introduces local variable
    //  by default, variables are immutable
    let hi = "hi";
    let mut count = 0;
    while count < 10 {
        println!("count is {}", count);
        count += 1;
    }
    //You can specify the type for a variable by following the name with a colon, then the type
    //  static items always require a type declaration
    //Subsequent lets can override previous variables
    static MONSTER_FACTOR: f64 = 57.8;
    let monster_size = MONSTER_FACTOR * 10.0;
    //This will throw a warning, since we don't use the float version
    //  if we are intentionally not using a variable, you can prefix it with _
    let monster_size: int = 50;
}

//Identifiers, by convention are lower case, underscore-separated words
//Types should be camel case

fn epxression_test(){
    let item = "foobar";

    let price;
    if item == "salad" {
        price = 3.50;
    }else if item == "muffin" {
        price = 2.25;
    } else {
        price = 2.00;
    }
    //You can do this without the name being repeated
    let price = 
        if item == "salad" {
            3.50
        }else if item == "muffin" {
            2.25
        } else {
            2.00
        };
    //Note the semicolon moved to the end, making the whole if/else block a single expression
    //If you have a semicolon after last entry in a block, the block returns () (nil or void)
}

fn is_four(x: int) -> bool {
    //No need for return statement. Result of expression
    //is used for return value
    x == 4
}

fn type_test() {
    //There are signed and unsigned integer types
    //  int, and uint... as well as 8-,16-,32-,and 64-bit variants,
    //  i8,u16,etc.
    //input bases: 
    //  decimal -- 144
    //  hex: 0x90
    //  octal 0o70
    //  binary: 0b10010000
    //you can use a suffix to specify signed/type
    let a = 1; // a is an int
    let b = 10i; // b is an int, due to 'i' suffix
    let c = 100u; // c is uint
    let d = 1000i32; // d is an i32
    //Two floating point types: f32 and f64
    //can do 0.0, 1e6, or 2.1e-4
    //  can also do type suffixes
    
    //Strings -- chars are 4-byte unicode codepoints
    let c1 = 'x';
    let tab = '\t';
    let newline = '\n';
    let s1 = "foobar";
    let s2 = "foo\nbar";
    let rawString = r##"blagh\nhaha"##; //need matching number of 0 or more #'s
}

fn operators() {
    //You can use 'as' to do compile time casting
    //arithmetic: * / % + -
    //bitwise: <<, >>, *, |, ^, and ! (invert)
    let x: f64 = 4.0;
    let y: uint = x as uint;
    assert!(y == 4u)
}

//Syntax extensions (macros) always end in !

//Conditionals:
//  if statement bodies require braces
//  conditions must evaluate to bool, no auto-conversion happens

fn pattern_match() {
    //pattern matches must be exaustive
    let my_number = 10;
    match my_number {
        //you can either use commas, or blocks, or both
        0 => println("zero"),
        1 | 2 => {println("one or two")}
        3..10 => {println("three to ten")}
        _ => {println("something else")}
    }
}

//See use fields at top of file
fn angle(vector: (f64, f64)) -> f64 {
    let pi = f64::consts::PI;
    let (a, b) = vector; // can destructure in a let as well
        //Can destructor tuples
        match vector {
            (0.0, y) if y < 0.0 => 1.5 * pi,
            (0.0, y) => 0.5 * pi,
            (x, y) => atan(y / x)
        }
}

fn loops() {
    let mut cake_amount = 8;
    while cake_amount > 0 {
        cake_amount -= 1;
    }
    
    //loop is an infinite loop, and is preferred way of doing while true
    //break and continue work as in C
    let mut x = 5u;
    loop {
        x += x - 3;
        if x % 5 == 0 { break; }
        println(x.to_str());
    }
}

//Structs
struct Point {
    x: f64,
    y: f64
}
//Structs have 'inherited mutability', which means that any field of a struct
//  may be mutable, if the struct is in a mutable slot
fn struct_mut() {
    let mut mypoint = Point { x: 1.0, y: 1.0 };
    let origin = Point { x: 0.0, y: 0.0};
    mypoint.x += 1.0; // struct is mutable, and it's fields
    //origin.y += 1.0; //error, can't assign to immutable field
    
    //match patterns destructure structs
    //basic syntax is: Name { fieldname: pattern, ... }
    match mypoint {
        Point { x: 0.0, y: yy } => { println(yy.to_str()); }
        Point { x: xx, y: yy } => { println(xx.to_str() + " " + yy.to_str()); }
    }
    //the field names of a struct don't have to appear in the same order they appear in the type
    //you can also match on some fields, by doing Name { field1: pattern1, .. }
    //the name of a field can be used as a var simply
    match mypoint {
        Point { y, .. } => { println(y.to_str()) }
    }
}

//enums are datatypes that have several alternative representations
enum Shape {
    Circle(Point, f64),
    Rectangle(Point, Point)
}
//They end up implemented like a tagged struct, but with compile time guarentees
//The above declaration will define a type Shape, and two functions, Circle and Rectangle
//This is how you use the functions:
fn enum_test(){
    let myCircle: Shape = Circle(Point { x: 0.0, y: 0.0}, 10.0);
}

//They don't need parameters (C-like)
enum Direction {
    North,
    East,
    South,
    West
}
//When they're C-like, you can explicitly set the constant values for it
enum ColorMask {
    Red = 0xff0000,
    Green = 0x00ff00,
    Blue = 0x0000ff
}
//If an explicit discriminator (value) is not set, it defaults to the value
//  of the previous one + 1, and starts at 0, ex: North is 0, East is 1, South is 2...
//When an enum is C-like, you can apply the as cast operator to convert it to it's' int

//For an enum type with multiple variants, destructuring is the only way to get the
//  contents
fn area(sh: Shape) -> f64 {
    match sh {
        Circle(_, size) => f64::consts::PI * size * size,
        Rectangle(Point { x, y }, Point { x: x2, y: y2 }) => (x2 - x) * (y2 - y)
    }
    //You can write a lone _ to ignore a single field, or to ignore all fields:
    //  you can do Circle(..)
}

fn point_from_direction(dir: Direction) -> Point {
    match dir {
        North => Point { x: 0.0, y: 1.0 },
        East => Point { x: 1.0, y: 0.0 },
        South => Point { x: 0.0, y: -1.0 },
        West => Point { x: -1.0, y: 1.0 },
    }
}

//Tuples are like structs, but their fields have no names
//  you can only access the fields via destructuring

//Tuple structs are like structs and tuples, but unlike tuples, they have names
//  so Foo(1, 2) is a different type than Bar(1, 2), the fields still have no names
fn struct_tup() {
    struct MyTup(int, int, f64);
    let mytup: MyTup = MyTup(10, 20, 30.0);
    match mytup {
        MyTup(a, b, c) => println!("{}", a + b + (c as int))
    }
}
//There's a special case for tuple structs with a single fields
//  they're called 'newTypes', after Haskell's newType feature
//They're used to define new types where it's not just a synonum, but it's
//  own distinc type
struct GizmoId(int);

//they can be used to differentiate between data that has the same underlying type,
//  but must be used in different ways
struct Inches(int);
struct Centimeters(int);
fn new_type_test(){
    let length_with_unit = Inches(10);
    let Inches(int_length) = length_with_unit;
    println!("length is {} inches", int_length)
}

//Functions that return nothing return nil, or ()
fn do_nothing_the_hard_way() -> () { return (); }
fn do_nothing_the_easy_way() {}

//Ending a function in a semicolon is the same as returning ()
fn line(a: int, b: int, x: int) -> int { a * x + b }
fn oops(a: int, b: int, x: int) -> () {a * x + b;}
//You can destructure in function definitions, as well
fn first((value, _): (int, f64)) -> int { value }

//Destructors
//  objects are never accessible after their destructor has been called, so no
//  dynamic failures are possible from accessing freed resources.
//when a task fails, destructors of all objects in the task are called
//the ~ prefix represents a unique handle for a heap allocation
fn heap_test(){
    //an integer allocated on the heap
    let y = ~10;
    //y is freed when we leave this function
}

//Ownership:
//the struct owns the objects contained in the x and y fields
struct Foo { x: int, y: ~int }
fn owner_test(){
    //a is the owner of the struct, and thus the fields in the struct
    let a = Foo { x: 5, y: ~10 };
    //when a goes out of scope, the ~int field's destructor is called
}
//if an object doesn't contain any non-Send types, it consists of a single
//  ownership tree, and is itself given the Send trait, which allows it
//  to be sent between tasks
//Custom destructors can only be implemented directly on types that are
//  send, but non-Send types can still contain types with custom destructors
//examples of types which are non-Send are Gc<T> and Rc<T>, the shared-ownership types 

//Implementing a linked list
// enum List{
//     Cons(...), //incomplete def
//     Nil // End of list
// }

//This won't work:
//  Cons(u32, List),
//  This is like storing a second list directly inside the first, not a pointer
//This is related to Rust's precise control over memory layout

//Boxing:
//A value in rust is stored directly inside the owner..
//  if a struct contains four u32 fields, it will be 4x the size of a single u32
fn list_test(){
    //This is deriving List from Clone, which allows .clone to be called on it
    //  Traits show up later
    #[deriving(Clone)]
    enum List {
        //~ is an 'owned box', on the heap and has a single owner
        Cons(u32, ~List),
        Nil
    }
    let list = Cons(1, ~Cons(2, ~Cons(3, ~Nil)));
    //since the list variable is immutable, the whole list is, as well
    
    //Copies will only copy as deep as the pointer to the box,
    //  rather than doing an implicit heap allocation
    let xs = list; //copies 'Cons(u32, pointer)' shallowly
    //However, after this line, the 'list' variable is not able to be used
    //  as Rust counts this as moving ownership
    //If the original is mutable, you can use it again after re-initializing it
    //To avoid a move, you can use the library-defined clone method
    let x = ~5;
    let y = x.clone(); //y is a newly allocated box, doesn't' move from x
    let z = x; //no new memory allocated, x can no longer be used
    
    //The mutability of a value may also be changed by moving it's variable
    let r = ~13;
    let mut s = r; //s is mutable, r is not usable
    *s += 1; //* is to dereference pointer
    let t = s; //it's now immutable again

    fn prepend(xs: List, value: u32) -> List {
        Cons(value, ~xs)
    }
    let mut zs = Nil;
    zs = prepend(zs, 1);
    zs = prepend(zs, 2);
    zs = prepend(zs, 3);
    //Ownership of the list is passed in, as opposed to just
    //  mutating in place
    
    //An obvious signature for a list equality comparision is the following:
    //fn eq(xs: List, ys: List) -> bool { ... }
    //However, this causes both lists to be 'move'd into the function,
    //  so you can't use them after
    //Since ownership isn't required to compare the lists, you can use
    //  references to them instead
    fn eq(xs: &List, ys: &List) -> bool{
        match (xs, ys) {
            //If we've reached the ends of the list, they're equal
            (&Nil, &Nil) => true,
            //If the current element in both is equal, continue
            (&Cons(ref x, ~ref next_xs), &Cons(ref y, ~ref next_ys))
                if x == y => eq(next_xs, next_ys),
            _ => false
        }
    }
    //Note: rust doesn't guarentee tail-call optimization, but
    //  llvm is able to handle a simple case like this with optimizations enabled
}

fn generic_list_test(){
    enum List<T>{
        Cons(T, ~List<T>),
        Nil
    }
    fn prepend<T>(xs: List<T>, value: T) -> List<T>{
        Cons(value, ~xs)
    }
    //Generics generate type-specific versions for each use-case in the program
    let mut xs = Nil; //Unknown type!, T could be anything
    xs = prepend(xs, 10); //the compiler looks here, and sees an int
    xs = prepend(xs, 15);
    xs = prepend(xs, 20);
    //Type annotations here were optional
    //It's the equiv. of doing this:
    let mut ys: List<int> = Nil::<int>;
    ys = prepend::<int>(ys, 10);
    ys = prepend::<int>(ys, 15);
    ys = prepend::<int>(ys, 20);
    //Types use Type<T, U, V> for list of type params, but
    //  identifiers use identifier::<T,U,V> to disambiguate the < operator
    
    //For equality, we have to have a limitation on the type of T in the list
    //<T: Eq> means T must have an implementation for Eq
    fn eq<T: Eq>(xs: &List<T>, ys: &List<T>) -> bool {
        match (xs, ys) {
            (&Nil, &Nil) => true,
            (&Cons(ref x, ~ref next_xs), &Cons(ref y, ~ref next_ys)) if x == y =>
                eq(next_xs, next_ys),
            _ => false
        }
    }
    
    //This would be a good time to go ahead and implement Eq for our list type:
    impl<T: Eq> Eq for List<T> {
        fn eq(&self, ys: &List<T>) -> bool {
            match (self, ys) {
                (&Nil, &Nil) => true,
                (&Cons(ref x, ~ref next_xs), &Cons(ref y, ~ref next_ys)) if x == y =>
                    eq(next_xs, next_ys),
                _ => false
            }
        }
    }
    
    let xs = Cons(5, ~Cons(10, ~Nil));
    let ys = Cons(5, ~Cons(10, ~Nil));
    //Like c, generate a pointer via &
    assert!(xs.eq(&ys));
    assert!(xs == ys);
    assert!(!xs.ne(&ys));
    assert!(!(xs != ys));
}

fn more_on_boxing() {
    fn foo() -> (u64, u64, u64, u64, u64, u64){
        (5, 5, 5, 5, 5, 5)
    }
    let x = ~foo(); //allocates a ~ box, and writes the ints directly into it
    
    //Beyond the properties granted by the size, an owned box behaves as a regular value
    // by inheriting the mutability and lifetime of owner
    let x = 5; //imm
    let mut y = 5; //mut
    y += 2;
    
    let x = ~5; //imm
    let mut y = ~5; //mut
    *y += 2; // * gets contained value
}

fn references(){
    struct Point{
        x: f64,
        y: f64
    }
    
    //These are all allocated in a different location
    let on_the_stack: Point = Point{x: 3.0, y: 4.0};
    let managed_box: @Point = @Point{x: 3.0, y: 4.0};
    let owned_box: ~Point = ~Point{x: 3.0, y: 4.0};

    //To have a fn take values but not copy them, you use references
    //  essentially, it's making sure it's a pointer, and 'borrowing' it
    fn compute_distance(p1: &Point, p2: &Point) -> f64 {
        let d_x = p1.x - p2.x;
        let d_y = p1.y - p2.y;
        sqrt(d_x * d_x + d_y * d_y)
    }
    //Now you can call it different ways:
    //need the & to turn on_the_stack to a pointer
    compute_distance(&on_the_stack, managed_box);
    compute_distance(managed_box, owned_box);
    
    //when a value is borrowed, you can't
    //  send the variable to another task, or take actions
    //  that would cause it to be freed or change it's type
    
    //freezing:
    //  lending an immutable pointer to an object freezes it and prevents mutations.
    //  Freeze objects have that freezing enforced at compile-time
    //an example of a non-Freeze type is RefCell<T>
    let mut x = 5;
    {
        let y = &x; //x is now frozen, it can't be modified
    }
    //x is now unfrozen again
}

fn dereferencing(){
    //NOTE -- @<value> to box managed values is deprecated
    //  Should be Using Gc and Rc for garbage collection and reference counting
    //  Haven't found guide on those yet
    //let managed = @10;
    //Gc is task-local
    let managed = Gc::new(10);
    //Rc expresses shared ownership over a reference counted box
    let managed_rc = Rc::new(20);
    let owned = ~20;
    let borrowed = &30;
    let sum = *managed.borrow() + *owned + *borrowed;
    
    //dereferenced mutable pointers may appear on the left hand of
    //  assignments, which modified value pointer points to
    let managed = Gc::new(10);
    let mut owned = ~20;
    let mut value = 30;
    let borrowed = &mut value;
    *owned = *borrowed + 100;
    *borrowed = *managed.borrow() + 1000;
    
    //dereferencing is lower precidence than the dot operator
    //  so accessing fields on pointers could be awkard...
    let start = Gc::new(Point { x: 10.0, y: 20.0});
    let end = ~Point{ x: start.borrow().x + 100.0, y: start.borrow().y + 100.0 };
    let rect = &Rectangle(*start.borrow(), *end);

    //`let area = (*rect).area();`
    //but the dot operator does dereferencing by default on pointers,
    //  so you can just do this:
    //`let area = rect.area();`
    
    //the indexing operator ([]) also auto-dereferences
}

fn vectors_and_strings(){
    //vector is a block of memory containing 0 or more items of the same type
    //strings are represented as vectors of u8, with the guarentee of containing
    //  a valid UTF-8 sequence
    
    //fixed-size vectors are an unboxed block of memory, with element length
    //  stored as part of the type
    //a fixed-size vector owns all of the elements it contains, so the elements
    //  in a vector are mutable if the vector itself is
    let numbers = [1, 2, 3];
    let more_numbers = numbers;
    //the type of a fixed-size vector is written as `[Type, ..length]`
    let five_zeros: [int, ..5] = [0, ..5];
    
    //Fixed size strings do not exist
    
    //a Unique vector is dynamically sized, and has a destructor to clean up
    //  after itself. it also own's it own elements
    let mut numbers = ~[1, 2, 3];
    numbers.push(4);
    numbers.push(5);
    //The type of a unique vector is ~[int]
    let more_numbers: ~[int] = numbers;
    
    //The original 'numbers' can't be used now, due to move semantics.
    let mut string = ~"fo";
    string.push_char('o');
    
    //Slices:
    //Slices are similar to fixed-size vectors, but don't store length
    //They simply point into a block of memory and don't have ownership over elements
    let xs = &[1, 2, 3];
    let ys: &[int] = xs;
    
    //Other vector types coerce to slices
    let three = [1, 2, 3];
    let zs: &[int] = three;
    
    //an unadorned string literal is an immutable string slice
    let string = "foobar";
    //A string slice type is written as &str
    let view: &str = string.slice(0, 3);
    
    //Mutable slices also exist, just as there are mutable references.
    //  However, there are no mutable string slices, due to multi-byte
    //  encoding of UTF-8
    let mut xs = [1, 2, 3];
    let view = xs.mut_slice(0, 2);
    view[0] = 5;

    //The type of a mutable slice is written as: &mut [T]
    let ys: &mut [int] = &mut [1, 2, 3];
    
    //Square brackets denote indexing into a vector
    /*
    let crayons: [Crayon, ..3] = [BananaMania, Beaver, Bittersweet];
    match crayons[0] {
        Bittersweet => draw_scene(crayons[0]),
        _ => ()
    }
    */
    
    //A vector can be destructures
    let score = match &[1, 2, 3] {
        [] => 0,
        [a] => a * 10,
        [a, b] => a * 6 + b * 4,
        [a, b, c, ..rest] => a * 5 + b * 3 + c * 2 + rest.len() as int
    };
    
    //Vectors and strings support a number of useful methods, defined in
    //   std::vec and std::str
}

fn rc_and_gc(){
    //For shared ownership over a box, you can use Rc or Gc, for
    //  reference counted or garbage collected, respectively
    //With shared ownership, mutability cannot be inherited, so boxes
    //  are always immutable.  However, it's possibble to use dynamic
    //  mutability via types like std::cell::Cell, where freezing is handled
    //  via dynamic checks and can fail at runtime
    //The Rc and Gc types are not sendable, so they cannot be used to share
    //  memory between tasks... Safe immutable and mutable sahred memory is
    //  provided by the extra::arc module.
    let x = Rc::new([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    let y = x.clone(); // a new owner
    let z = x; //this moves x into z, rather than creating a new owner

    //assert_eq!(*z.deref(), [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    
    //the variable is mutable, but not the contents
    let mut a = Rc::new([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    a = z;
    
    //Garbage collected boxes
    let x = Gc::new([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    let y = x; //does not perform a move, unlike with 'rc'
    let z = x;
    
    //assert_eq!(*z.borrow(), [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
}

fn closures(){
    //normally, a function doesn't close over locals in it's surrounding scope
    //However, you can do closures explicitly
    fn call_closure_with_ten(b: |int|) { b(10); }
    let captured_var = 20;
    //closures begin with the argument list between pipes, and a single expression
    //  remember, though, code blocks are considered single expressions
    //You can, if needed/wanted type-annotate the closure arg types and return types
    let square = |x: int| -> uint { (x * x) as uint };
    let closure = |arg| println!("captured_var={}, arg={}", captured_var, arg);
    call_closure_with_ten(closure);
    
    //There are several forms of closure, each with it's own role.
    //The most common, called a stack closure has type || and can directly
    //  access local vars
    let mut max = 0;
    [1, 2, 3].map(|x| if *x > max { max = *x});
    //Stack closures are very efficient because their environment is allocated on the
    //  call stack and refers by pointer to captured locals
    //To ensure stack closures never outlive the local variables to which they refer,
    //  they are not first class, they can only be used in argument position; they can
    //  not be stored in data structures, or returned from functions.
    //Despite that, they are used pervasively in Rust code
    
    //Owned closures
    //owned closures, written 'proc', hold on to things that can safely be sent
    //  between processes.  they copy the values they close over, much like managed
    //  closures, but they also own them.  frequently used for spawning tasks
    
    //all closures can be passed where the type is specified as ||
    //  which you can do as long as you only call it, and don't return it or do
    //  anything else to it
    fn call_twice(f: ||) { f(); f(); }
    let closure = || { "I'm a closure, and it doesn't matter what type I am."; };
    fn function() { "I'm a normal function"; }
    call_twice(closure);
    call_twice(function);
}

fn do_syntax(){ //The do syntax has been deprecated
    fn call_it(op: proc(v: int)) {
        op(10)
    }
    //as a caller, if we use a closure to provide the final operator arg, we
    //  can write it as a block structure:
    call_it(proc(n) {
        println(n.to_str());
    });
    //this is such a useful pattern, that there is a special form for it
    /*do call_it() |n| { // --DEPRECATED--
        println(n.to_str());
    }*/
    //the call is prefixed by do, and instead of passing the procedure in the
    //  argument list, it appears outside the parens, where it looks like
    //  a typical block of code
    
    //convenient way to create tasks with the task::spawn function
    //  spawn has the signature: spawn(fn: proc()), in otherwords, it
    //  takes an owned closure with no args
    /*do spawn() || { 
        debug!("I'm a task, whatever");
    }*/
    //the empty args lists can be omitted (both () and ||)
    /*do spawn {
        debug!("Kablam!");
    }*/
}

fn methods(){
    enum Shape {
        Circle(Point, f64),
        Rectangle(Point, Point)
    }
    impl Shape{
        fn draw(&self){self.draw_reference();}
        fn draw_reference(&self){
            match *self {
                Circle(p, r) => println("Circle!"),
                Rectangle(p1, p2) => println("Rectangle!")
            }
        }
    }
    
    let s = Circle(Point { x: 1.0, y: 2.0}, 3.0);
    s.draw();
    
    //self is a special name for an argument
    //You can also do &self, @self, ~self, and just self

    //The compiler will automatically convert to reference when you have
    //  a managed or unique pointer
    let gc_s = Gc::new(s);
    gc_s.borrow().draw_reference();
    (~s).draw_reference();
    //unlike typical fn arguments, self will auto-reference
    s.draw_reference();
    //And dereferenced
    (& &s).draw_reference();
    
    struct Circle { radius: f64 }
    //Static methods:
    impl Circle{
        //static
        fn new(area: f64) -> Circle { Circle{ radius: (area / f64::consts::PI).sqrt() } }
        fn area(&self) -> f64 {f64::consts::PI * self.radius * self.radius}
    }
    //To call static method, prefix with type name and double colon
    let c = Circle::new(42.5);
}

fn generics(){
    //Usually values are passed to function via pointer/reference
    //  which is why function is v: &T, and not v: T
    fn map<T, U>(vector: &[T], function: |v: &T| -> U) -> ~[U]{
        let mut acc = ~[];
        for element in vector.iter() {
            acc.push(function(element));
        }
        return acc;
    }
    
    //Generic type, struct, and enum declarations follow the same pattern
    type Set<T> = HashMap<T, ()>;
    struct Stack<T>{
        elements: ~[T]
    }

    /*enum Option<T>{
        Some(T),
        None
    }*/
    
    fn radius(shape: Shape) -> Option<f64> {
        match shape {
            Circle(_, radius) => Some(radius),
            Rectangle(..) => None
        }
    }
}

//Traits are like interfaces for Java, or typeclasses for Haskell
//  They allow you to do structural polymorphism
//This won't compile:
//  fn head_bad<T>(v: &[T]) -> T {
//      v[0] //error: copying a non-copyable value
//  }
//The bounded type param: T: Clone says that this will work for any T,
//  as long as there's an implementation of the Clone trait for T
fn head_mine<T: Clone>(v: &[T]) -> T {
    v[0].clone()
}

//While most traits can be defined and implemented by user code, three
//  traits are automatically derived and implemented for all applicable
//  types, and cannot be overridden:
//Send - Sendable types, applies unless they contain managed boxes,
//  managed closures, or references
//Freeze - Constant (immutable types). Don't contain anything intrinsically mutable.
//'static - non-borrowed types -- don't contain any data whose lifetime is bound
//  to a particular stack frame (no references, or where all referencces have 'static
//  lifetime)

//Drop -- trait to define destructors, via method drop
//Note: it's illegal to call drop directly
struct TimeBomb{
    explosivity: uint
}
impl Drop for TimeBomb{
    fn drop(&mut self){
        for _ in range(0, self.explosivity){
            println("blam!");
        }
    }
}

//Traits don't have to provide methods, they can also be used
//  just as classification, like Send and Freeze

fn trait_test(){
    trait Printable{
        //fn print(&self); //can also have no default implementation
        fn print(&self) { println!("{:?}", *self)}
    }
    
    //This just uses all the defaults
    impl Printable for int {}
    impl Printable for ~str {
        fn print(&self) { println(*self) }
    }
    impl Printable for bool {}
    impl Printable for f32 {}
    
    impl Printable for i32 {
        fn print(&self) { println("i32: " + self.to_str()); }
    }
    
    //Traits can have parameters, as well
    trait Seq<T>{
        fn length(&self) -> uint;
    }
    impl<T> Seq<T> for ~[T]{
        fn length(&self) -> uint { self.len() }
    }
    
    //Self is a special type within a trait definition
    trait Equality{
        fn equals(&self, other: &Self) -> bool;
    }
    impl Equality for int{
        fn equals(&self, other: &int) -> bool { *other == *self }
    }
    
    //traits can also define static methods
    trait Shape { fn new(area: f64) -> Self; }
    struct Circle { radius: f64 }
    struct Square { length: f64 }
    impl Shape for Circle {
        fn new(area: f64) -> Circle { Circle { radius: (area / PI).sqrt() } }
    }
    impl Shape for Square {
        fn new(area: f64) -> Square { Square { length: (area).sqrt() } }
    }
    let area = 42.5;
    let c: Circle = Shape::new(area);
    let s: Square = Shape::new(area);
    
    //Traits let us define bounds on a generic type
    fn print_all<T: Printable>(printable_things: ~[T]) {
        for thing in printable_things.iter() {
            thing.print();
        }
    }
    //Traits can have multiple bounds, by separating them by +
    fn print_all_clones<T: Printable + Clone>(printable_things: ~[T]){
        let mut i = 0;
        while i < printable_things.len() {
            let copy_of_thing = printable_things[i].clone();
            copy_of_thing.print();
            i += 1;
        }
    }
    //Method calls to bounded type params are statically dispached, imposing
    //  no more overhead than normal function invocation
    
    //In order to work on a list of arbitrary Printable's, you can use
    //  the trait as a type, called an object
    //This will do dynamic dispatch, note that different pointer types
    //  won't be able to be used in place of the requested one
    //  ex: ~Printable here can't be sent a &Printable
    fn print_all_any(printables: &[~Printable]){
        for printable in printables.iter() { printable.print(); }
    }
    let i1: ~int = ~32;
    let f1: ~f32 = ~10.5;
    print_all_any([i1 as ~Printable, f1 as ~Printable]);
    
    //Owned object
    let owny: ~Printable = ~42 as ~Printable;
    let stacky: &Printable = &42 as &Printable;
    
    //Owned traits (~Trait) must fulfill the Send bound
    //contents of reference traits (&Trait) aren't constrained
    //~Trait:Send+Freeze means the trait must fulfill Send and Freeze
    //~Trait:Send is equivalent to ~Trait
    //&Trait: is equivalent to &Trait
    
}
fn trait_inherit(){
    //Trait inheritance:
    trait Shape {fn area(&self) -> f64; }
    trait Circle : Shape { fn radius(&self) -> f64; }
    //Now we can implement Circle on a type only if we also implement Shape
    struct CircleStruct { center: Point, radius: f64 }
    impl Circle for CircleStruct {
        fn radius(&self) -> f64 { (self.area() / PI).sqrt() }
    }
    impl Shape for CircleStruct {
        fn area(&self) -> f64 { PI * self.radius * self.radius }
    }
    
    //You can use any supertrait fields on a trait object

    //A small number of traits in std and extra can have implementations
    //  that are automatically derived.
    #[deriving(Eq)]
    struct Circle2 { radius: f64 }
    
    #[deriving(Clone, Show)]
    enum ABC { A, B, C }
    
    //Derivable traits are: Eq, TotalEq, Ord, TotalOrd, Encodable, Decodable, Clone,
    //  DeepClone, Hash, Rand, Default, Zero, FromPrimitive, and Show
}

//Crates

//This is in the crate root
mod farm {
    use std::io::println;
    //This is the body of the module 'farm', declared in the root crate
    pub fn chicken(){println("Buckah!!!");}
    pub fn cow(){println("mooo");}
    
    mod barn {
        use std::io::println;
        //body of module barn
        fn hay(){println("...");}
    }
}

fn use_mod(){
    ::farm::chicken(); //this wouldn't compile if chicken wasn't declared pub
    //::farm::chicken is a path
    //:: prefix means global path
}

//visibility in Rust only exists at module boundries
//By default, fn, struct, static, mod, etc are private
//  Fields however, are public if the struct is
//   --they can be flagged as 'priv'
//If you declare a module without a body, the compiler will look in:
//  mod foo;
//  //looks for: foo.rs or foo/mod.rs
//in short, mod foo; is sugar for { /* content of <...>/foo.rs or <...>/foo/mod.rs */ }
//Inside the file farm.rs, the module farm is in the crate root
//You can also annotate a mod path
//#[path="../../area51/alien.rs"]
//mod classified;

//You can import symbols at the beginning of a module body, fn, or any other block
//  you can do 'use' modules with a global path with no prefix
//  in a 'use', you can do from the current module or from the parent via self:: and super::
//    use super::some_parent_item;
//    use self::some_child_module::some_item;
//Local definitions override imports
//You can import multiples using <base>::{<a>, <b>};
//  ex: use farm::{chicken, cow};
//And import all via:  use farm::*;
//  note -- this is behind a feature gate #[feature(globs)]
//You can rename imports, as well
//  use egg_layer = farm::chicken;
//You can also re-export names to be accessible from your module
mod other_farm{
    use std::io::println;
    pub use self::barn::hay;
    pub fn chicken() {println("other cluck?");}
    mod barn{
        use std::io::println;
        pub fn hay() {println("... again?");}
    }
}
fn pub_use_test(){
    other_farm::chicken();
    other_farm::hay();
}

//libraries:
//Looks for binary providing library
//extern mod extra;
//extern mod looks lik the library search path, which you can extend with -L flag
//  rust also ships with rustpkg, which lets you download packages automatically
//extren mod rust = "github.com/mozilla/rust"; // pretend Rust is a simple library

//Crate metadata
//For every crate, you can define a number of metadata items, such as link name, version
//  or author.  You can also toggle settings that have crate-global consequenses

//Rust uniquely identifies crates by their link metadata
//  allowing you to use two versions of a library with no conflict
//#[crate_id = "farm#2.5"];
//You can also specify package ID info in an extern mod statement
//  ex: these statements would both accept and select the crate defined above:
//    extern mod farm;
//    extern mod farm = "farm#2.5"
//    extern mod my_farm = "farm"

//You can set the crate type (lib or executable) explicitly
//  #[crate_type = "lib"];
//Turn on a warning
//  #[warn(non_camel_case_types)]


//Simple mod example:

//world.rs
//  #[crate_id = "world#0.42"];
//  pub fn explore() -> &'static str { "world" }

//main.rs
//  extern mod world;
//  fn main() { println("hello " + world::explore()); }

//To compile:
//  rustc --lib world.rs # compiles libworld-<HASH>-0.42.so
//  rustc main.rs -L .   # compiles main

//std functions
//rust automatically inserts:
//  extern mod std;
//  use std::prelude::*;

//prelude re-exports common definitions from std

//You can turn off the the auto-insertions, as well
//#[no_std];
//#[no_implicit_prelude];

//Pointer use cases:
//1. Owned: ~Trait must be a pointer, because you don't know the size of the object
//2. Owned: you need a recursive data structure
//3. Owned: Passing a large amount of data
//4. Managed: having a shared data
//5. Reference: In a fn, need a pointer, but don't care about ownership

//Keep in mind for smaller structs, it may be more efficient to pass in by
//  value rather than via pointer

//Owned pointers:
//-Only one owned pointer may exist to a particular place in memory. It may be borrowed
//-Rust compiler uses static analysis to de-allocate when needed
//Main use Cases
//-Recursive data structures (ex: Cons list)
//-Efficiency -- It may be more efficient to pass a pointer than a value
//-References to traits -- due to not knowing size of trait

//Managed Pointers:
//Via std::rc::Rc and std::gc::Gc
//Allow multiple owners, but can't share across tasks

//References:
//-Only borrow the data, don't own it
//--That means you can't modify it

//Returning pointers:
//  Generally, only return a unique or managed pointer if you were given one
//    in the first place
fn ret_point(){
    //Instead of this....
    fn foo(x: ~int) -> ~int{
        return ~*x;
    }
    fn use_foo(){
        let x = ~5;
        let y = foo(x);
    }
    
    //do this
    fn bar(x: ~int) -> int{
        return *x;
    }
    fn use_bar(){
        let x = ~5;
        let y = ~foo(x);
    }
    //This allows you to use any pointer type to return the value
    //  The compiler automatically handles the storage of the return into the pointer,
    //  making it the same efficiency
    //Pointers are not for optimizing return values from your code.
    //  allow the caller to choose how they want to use the output
}

//Returning references
fn get_x<'r>(p: &'r Point) -> &'r f64 { &p.x }
//the 'r names the lifetime of the pointer explicitly.
//  This function declares that it takes a pointer with lifetime r and returns
//    a pointer with the same lifetime
//In general, it's only possible to return references if they are derived from a parameter
//  to the function.  In that case, the pointer result will always have the same lifetime
//  as one of the parameters, named parameters indicate which param that is
//The lifetime parameter is defined by the caller of get_x, not the function itself
//Essentially, the pointer produced by &p.x has the same lifetime as p itself

//Named lifetimes can be used to group parameters by lifetime
/*
fn select<'r, T>(shape: &'r Shape, threshold: f64,
                 a: &'r T, b: &'rT) -> &'r T{
    if compute_area(shape) > threshold {a} else {b}
}
*/
//In this function, in the caller, the lifetime r will be the intersection of all three
//  of the named lifetime parameters
fn lifetime_test(){
    /*
    fn minLife(x: &int, y: &int) -> &int {
        if(*x < *y){
            //Is the lifetime for x?
            x
        }else{
            //or for y?
            y
        }
    }
    */
    fn minLife<'a>(x: &'a int, y: &'a int) -> &'a int{
        //Now the lifetime of both params, and the return is the
        //  intersection of their original lifetimes
        if *x < *y {
            x
        }else{
            y
        }
    }
}

//Tasks:
//Lightweight thread, even main function runs in one
//  dynamic stack sizes, protects against overruns
//Tasks provide failure isolation and recovery... when a fatal error occurs in Rust code,
//  (via fail!(), assertion failure, or another invalid operation), the runtime system
//  destroys the task.  There's no way to catch an exception, but tasks may monitor each
//  other for failure
//Tasks cannot share mutable state with each other, they communicate with each other by
//  transferring owned data through the global 'exchange heap'

//At it's simplest, creating a task is a matter of calling 'spawn' with a closure argument
fn task_test_1(){
    use sync::DuplexStream;
    fn print_message(){println("In a different task!");}
    spawn(print_message);
    
    //print something more profound in a different task using a lambda
    spawn(proc() println("I'm also in another task"));
    
    //The canonical way to spawn is using 'do' notation
    //Deprecated
    /*do spawn {
        println("Another task too?");
    }*/
    spawn(proc() {println("Another task too?");});
    
    fn gen_task_number() -> int { 5 }
    let child_task_number = gen_task_number();
    spawn(proc() {
        //This variable is now owned by the closure, and not the parent
        println!("I'm task number {}", child_task_number);
    });
    
    //Communication
    //Channel is sending side, Port is receiving endpoint
    let (sender, receiver): (Sender<int>, Receiver<int>) = channel();
    spawn(proc() {
        //do expensive stuff
        let result = 5;
        //This captures chan from the caller implicitly
        sender.send(result);
    });

    //do other expensive stuff
    let result = receiver.recv();
    
    //You can, however use a SharedChan to have multiple senders
    let (sender, receiver) = channel();
    for init_val in range(0u, 3){
        let child_sender = sender.clone();
        spawn(proc() {
            child_sender.send(5)
        })
    }
    
    let result = receiver.recv() + receiver.recv() + receiver.recv();
    
    //SharedChan is a non-copyable, owned type, (aka: affine or linear)
    //  Unlike with Chan, you can .clone() it
    
    //Futures allow you to compute something and retrive the value later
    fn fib(n: u64) -> u64 {
        //lengthy computations returning a uint
        12586269025
    }
    
    let mut delayed_fib = sync::Future::spawn(proc() fib(50));
    //Do some expensive stuff
    println!("fib(50) = {:?}", delayed_fib.get());
    //<future>.get will block until the value is available
    
    //You can use futures to split up computations onto multiple cores
    fn partial_sum(start: uint) -> f64{
        let mut local_sum = 0f64;
        for num in range(start*100000, (start+1)*100000){
            local_sum += f64::pow((num as f64 + 1.0), -2.0);
        }
        local_sum
    }
    fn do_sum(){
        use std::vec;
        let mut futures = Vec::from_fn(1000,
                                       |ind| sync::Future::spawn(proc() { partial_sum(ind) }));
        let mut final_res = 0f64;
        for ft in futures.mut_iter() {
            final_res += ft.get();
        }
        println!("n^2/6 is not far from: {}", final_res);
    }
    
    //To share data without copying it, we can use Arcs in the extra library
    fn arc_test(){
        use sync::Arc;
        
        fn pnorm(nums: &Vec<f64>, p: uint) -> f64 {
            nums.iter().fold(0.0, |a,b| f64::pow(f64::pow(a+(*b), (p as f64)), (1.0 / (p as f64))))
        }
        
        fn driver(){
            use std::vec;
            let numbers = Vec::from_fn(1000000, |_| rand::random::<f64>());
            //Can't use max with f64, as they don't currently implement TotalOrd
            //println!("Inf-norm = {}", *numbers.iter().max().unwrap());
            
            let numbers_arc = Arc::new(numbers);
            for num in range(1u, 10){
                let (sender, receiver) = channel();
                //Send a clone of the arc (each referring to the same data), to each task
                sender.send(numbers_arc.clone());
                
                spawn(proc() {
                    let local_arc : Arc<Vec<f64>> = receiver.recv();
                    let task_numbers = &*local_arc;
                    println!("{}-norm = {}", num, pnorm(task_numbers, num));
                });
            }
        }
    }
    
    //Handling task failure:
    //It's not possible to recover from a failure, but you can notify another task of the failure
    //The simplest way is with the 'try' function, which is similar to spawn, but immediately blocks
    //  waiting for the task to finish... It returns a value of type Result<T, ()>, result is an enum
    //  with two variants: Ok, and Err -- you can use pattern matching to extract the result from Ok
    //Currently, no data is returned from an error, in the future that may be possible
    /*
    let result: Result<int, ()> = do task::try{
        if some_condition() {
            calculate_result()
        }else{
            fail!("oops!");
        }
    };
    assert!(result.is_err());
    */
    
    //Bi-directional communication
    //ex: a child that takes in ints, and returns the string rep of it
    fn stringifier(channel: &DuplexStream<~str, uint>){
        let mut value: uint;
        loop{
            value = channel.recv();
            channel.send(value.to_str());
            if value == 0 { break ;}
        }
    }
    
    fn use_stringifier(){
        let (from_child, to_child) = sync::duplex();
        spawn(proc() {
            stringifier(&to_child);
        });
        
        from_child.send(22);
        assert!(from_child.recv() == ~"22");
        
        from_child.send(23);
        from_child.send(0);
        
        assert!(from_child.recv() == ~"23");
        assert!(from_child.recv() == ~"0");
    }
}

//======Stopped at chapter 19, Tasks and comm.====

fn macro_test(){
    //ex: you have this code
    /*
    match input_1 {
        special_a(x) => { return x; }
        _ => {}
    }
    match input_2 {
        special_b(x) => { return x; }
        _ => {}
    }
    */
    macro_rules! early_return(
        ($inp:expr $sp:ident) => ( //invoke it like `(input_5 special_e)`
            match $inp {
                $sp(x) => {return x;}
                _ => {}
            }
        );
    );
    //then use like so:
    //  early_return!(input_1 special_a);
    //  early_return!(input_2 special_b);
    //  etc...

    //Invokation syntax of macro: (before the => ... ex: ($inp:expr $sp:ident))
    //-must be surrounded by parens
    //-$ has special meaning
    //-[], (), and {} must be balanced
    
    //To take a fragment of rust code as an arg, write $<name>:<specifier>
    //specifiers:
    //  ident -- identifier, a variable or item (ex: f, x, foo)
    //  expr -- epxression, ex: 2 + 2; if ture then {1} else {2}; f(42)
    //  ty -- type, ex: int, ~[(char, ~str)], &T
    //  pat -- pattern, usually appearing in a match or lhs of declaration
    //         ex: Some(t), (17, 'a'), _
    //  block -- sequence of actions. ex: { log(error, "hi"); return 12; }
    //The parser interprets any token that's not preceded by $ literally
    //  so ($x:ident -> (($e:expr))), though excessivly fancy, would designate
    //  a macro that could be invoke like: my_macro!(i->(( 2+2)))
    
    //Transcription syntax (after the =>)
    //- must be enclosed by delimiters, therefore
    //    () => ((1, 2, 3)) is a macro that expands to a tuple
    //    () => (let $x=$val) is a macro that expands to a statement
    //    () => (1, 2, 3) is a macro that expands to a syntax error
    //        -- essentially converts to code '1, 2, 3'
    
    //Handling multiple inputs
    macro_rules! early_return2(
        ($inp: expr, [ $($sp:ident)|+]) => (
            match $inp {
                $(
                    $sp(x) => {return x;}
                )+
                _ => {}
            }
        );
    );
    //invoke like:
    //  early_return2!(input1, [special_a|special_b|special_d]);
    //  early_return2!(input2, [special_b]);
    
    //$(...)* and $(...)+ mean accept zero|one or more occurences of it's contents
    //  It also supports a separator token, ex: $(...),* for a comma separated list
    
    //Must show up last, or have a separator
    //'$($t:ty)* $e:expr' will fail, because the parser won't know when to stop the ty's
    //'$($t:ty)* E $e:expr' will work, due to the separator
}

fn containers(){
    use collections::hashmap::{HashSet, HashMap}; //requires keys to implement Eq and Hash
    use collections::trie::{TrieMap, TrieSet}; // requires keys to be uint
    use collections::treemap::{TreeMap, TreeSet}; // requires keys to implement TotalOrd
    //These maps don't use managed pointers, so they can be sent between tasks
    //Rust provides SipHash implementation for any type implementing IterBytes
    
    //extra::ringbuf module implements a double ended queue with O(1) inserts and removals
    //  also has O(1) indexing like a vector
    //It's interface Deque is defined in extra::collections
    //extra::dlist has a double-ended linked list

    //extra::priority_queue implements a queue ordered by key
    
    //Iterators in std::iter
    //Minimal implementation is a next method, yielding next element from iterator
    struct ZeroStream;
    impl Iterator<int> for ZeroStream{
        fn next(&mut self) -> Option<int>{
            Some(0)
        }
    }
    //Reaching the end is signalled by returning None instead of some
    struct NZeroStream{
        //Normally would be 'priv', but inside fn has no effect
        remaining: uint
    }
    impl NZeroStream{
        fn new(n: uint) -> NZeroStream{
            NZeroStream{ remaining: n }
        }
    }
    impl Iterator<int> for NZeroStream{
        fn next(&mut self) -> Option<int>{
            if self.remaining <= 0 {
                None
            } else {
                self.remaining -= 1;
                Some(0)
            }
        }
    }
    
    //In general, you can't rely on the behavior of next() after it's returned None
    
    //Most containers implement the following for iters
    //iter() and rev_iter(), for immutable references
    //mut_iter() and mut_rev_iter(), for mutable references to elements
    //move_iter() and move_rev_iter(), to move elements out by-value
    
    //Rust has no iterator invalidation, as long as an iterator is still
    //  in scope, the compiler will prevent modification of the container
    //  through another handle
    let mut xs = [1, 2, 3];
    {
        let _it = xs.iter();
        //The vector is frozen for this scope, the compiler will prevent
        //  modification
    }
    //The vector is now unfrozen again
    
    //The iterator trait provides many common algorithms as default methods.
    //  ex: the fold method will accumulate the items in an Iterator into a single value
    
    //To make sure an iterator never calls anything after it has returned None, you can fuse it
    let xs = [1,2,3,4,5];
    let mut calls = 0;
    let it = xs.iter().scan((), |_, x| {
        calls += 1;
        if *x < 3 {Some(x)}else{None}});
    //The iterator will only yield 1 and 2 before returning None
    //If we were to call it 5 times, calls would end up at 5, despite only 2 values
    //  being returned
    let mut it = it.fuse();
    it.next();
    it.next();
    it.next();
    it.next();
    it.next();
    //assert_eq!(calls, 3);
    
    //the function range, or range_inclusive allows you to simply iterate through a given range
    for i in range(0, 5){
        print!("{} ", i) //0 1 2 3 4
    }
    for i in std::iter::range_inclusive(0, 5) { //needs explicit import
        print!("{} ", i) //0 1 2 3 4 5
    }
    
    //the for keyword can be used as sugar for iterating through any iterator
    let xs = [2u, 3, 5, 7, 11, 13, 17];
    for x in xs.iter(){
        println(x.to_str())
    }
    //print all but the first 3 elements
    for x in xs.iter().skip(3) {
        println(x.to_str())
    }
    
    //For loops are often used with a temp iterator object, as above
    //  they can also advance the state of an iterator in a mutable location
    let xs = [1,2,3,4,5];
    let ys = ["foo", "bar", "baz", "foobar"];
    //create an iterator yielding tuples of elements from both vectors
    let mut it = xs.iter().zip(ys.iter());
    //print out the pairs up to (&3, &"baz")
    for(x, y) in it{
        println!("{} {}", *x, *y);
        if *x == 3{
            break;
        }
    }
    //yield and print the last pair from the iterator
    println!("last: {:?}", it.next());
    //The iterator is now fully consumed
    assert!(it.next().is_none());
    
    //Iterators offer generic conversion to contains with the collect adapter:
    let xs = [0,1,1,2,3,5,8];
    //Need a type hint for the container type
    let ys = xs.rev_iter().skip(1).map(|&x| x * 2).collect::<~[int]>();
    //assert_eq!(ys, ~[10, 6, 4, 2, 2, 0]);
    
    //Containers can provide conversion from iterators through collect by implementing the FromIterator
    //  trait.. ex:
    //Following is the implemtation for vectors
    /*
    impl<A> FromIterator<A> for ~[A]{
        pub fn from_iterator<T: Iterator<A>>(iterator: &mut T) -> ~[A]{
            let (lower, _) = iterator.size_hint();
            let mut xs = std::vec::with_capacity(lower);
            for x in iterator{
                xs.push(x);
            }
            xs
        }
    }
    */
    
    //Size hints
    //  The iterator trait provides a size_hint default method, returning a lower bound
    //  and optionally an upper bound on the length of the iterator
    //It's always correct, but if you can provide a length, you might as well
    //For NZeroStream, it would be done like so:
    /*
    fn size_hint(&self) -> (uint, Option<uint>) {
        (self.remaining, Some(self.remaining))
    }
    */
    
    //Double ended iterators
    //The DoubleEndedIterator trait represents an iterator able to yield elements from either
    //  end of a range
    //It inherits from the Iterator trait and extends it with the next_back function
    //It can be flipped with the invert adapter, returning another DoubleEndedIterator
    //  with next and next_back exchanged
    
    //The rev_iter and mut_rev_iter on vectors just return an inverted version of the
    //  standard immutable and mutable vector iterators
    
    //The chain, map, filter, filter_map, and inspect adaptors are DoubleEndedIterator implementations
    //  if the underlying iterators are
    
    //Random access iterators:
    //The RandomAccessIterator trait provides indexable (number of elements accessable with idx) 
    //  and idx methods
    //The chain adaptor is an implementation of RandomAccessIterator if the underlying iterators are
    let xs = [1,2,3,4,5];
    let ys = ~[7,9,11];
    let mut it = xs.iter().chain(ys.iter());
    println!("{:?}", it.idx(0)); //prints `Some(&1)`
    println!("{:?}", it.idx(5)); //prints `Some(&7)`
    println!("{:?}", it.idx(7)); //prints `Some(&11)`
    println!("{:?}", it.idx(8)); //prints `None`
    
    //Take two elements from the beginning, one from the end
    it.next();
    it.next();
    it.next_back();
    println!("{:?}", it.idx(0)); //prints `Some(&3)`
    println!("{:?}", it.idx(4)); //prints `Some(&9)`
    println!("{:?}", it.idx(6)); //prints `None`
}

//Read in a file with two columns of numbers, and print them out
fn error_handling(path: &str){
    use std::io::BufferedReader;
    use std::io::File;

    fn read_int_pairs(path: &str) -> ~[(int, int)]{
        let mut pairs: ~[(int, int)] = ~[];
        //Path takes a generic by-value, rather than by-reference
        let path = Path::new(path);
        let mut reader = BufferedReader::new(File::open(&path));
        let default_line = ~"";
        //1. Iterate over lines of file:
        for line in reader.lines(){
            //2. Split lines into fields ("words")
            let line_contents = line.or(Ok(default_line.clone())).unwrap();
            let fields: Vec<&str> = line_contents.words().collect();
            //3. Match the vector of fields against a vector pattern
            match fields.slice(0, fields.len()) {
                //4. When the line had two fields...
                [a, b] => {
                    //5. Try parsing from both fields as ints
                    match (from_str::<int>(a), from_str::<int>(b)){
                        //6. If parsing succeeded for both, push both
                        (Some(a), Some(b)) => pairs.push((a, b)),
                        //7. Ignore non-int fields
                        _ => ()
                    }
                },
                //8. Ignore lines that don't have two fields
                _ => ()
            }
        }
        
        pairs
    }
    //this example uses Option for error handling
    //Options have a few drawbacks:
    //  -need to use either matching or Option::unwrap to get value
    //  -caller must handle errors
    //  -collapses all errors into one (no specific error message)
    
    //Result<T, E> (like Either in scala)
    //Two forms: Ok(T), and Err(E)
    //ex: from_str could have done like so:
    enum IntParseErr {
        EmptyInput,
        Overflow,
        BadChar(char)
    }
    fn from_str_refactored(s: &str) -> Result<int, IntParseErr>{
        //This would do some processing
        Ok(5)
    }
    
    //The third and arguably easiest way to handle errors is called 'failure'
    //ex: if you unwrap an option, and it's a None, it calls:
    //  fail!("option::unwrap `None`")
    //Every rust task can fail, either indirectly due to a kill signal or other
    //  async event, or by failing an assert! or calling fail!
    //Failure is unrecoverable at the task level
    //Failure is:
    // -simple and terse -- good for when you can't continue past error
    // -All errors (except memory safety ones) can be uniformly trapped by
    //    a supervisory task outside the failing task
    // -A blunt instrument, terminates the containing task entirely
    
    //You can 'catch' a failure by using task::try
    fn read_int_pairs_fail(path: &str) -> ~[(int, int)]{
        let mut pairs = ~[];
        let path = Path::new(path);
        let mut reader = BufferedReader::new(File::open(&path));
        for line in reader.lines(){
            //Have to store this in a temp variable so that it will have enough of
            //  a lifetime to be used
            let line_text = line.or(Ok(~"")).unwrap();
            let line_words: Vec<&str> = line_text.words().collect();
            match line_words.slice(0, line_words.len()) {
                [a, b] => pairs.push((from_str::<int>(a).unwrap(),
                                      from_str::<int>(b).unwrap())),
                //Explicitly fail on malformed lines
                _ => fail!("Invalid line (needs to be <int> <int>)")
            }
        }
        pairs
    }
    fn run_read_int_pairs_fail(path: &str){
        use std::task;
        //Have to copy string into new storage to send to task
        let owned_path = path.to_owned();
        //Isolate failure within subtask
        let result = task::try(proc() {
            //The protected logic
            let pairs = read_int_pairs_fail(owned_path);
            for &(a, b) in pairs.iter(){
                println!("{:4.4d}, {:4.4d}", a, b);
            }
        });
        if result.is_err(){
            println("parsing failed");
        }
    }
}
    
//Conditions have been removed from rust
/*
//Create the condition
condition! {
    //You can also have this return, for example, an Option<(int, int)>, or
    //  an Enum with options like UsePair(int, int), IgnoreLine, UsePreviousLine, etc.
    pub malformed_line : ~str -> (int, int);
}

condition! {
    pub malformed_int : ~str -> int;
}

fn conditions(){
    //Conditions
    //  designed to trike a balance between failure and option/result
    //Like exceptions and failure, conditions separate the site where the error is raised
    //  and the site where it's trapped
    //Unlike exceptions/failure, when a condition is raised/trapped, no unwinding occurs
    //Successfully trapped condition causes execution to continue 'at the site of the error'
    //  as though no error occurred
    
    //declared with condition! macro, they each have a name, an input type and output type
    //  much like a function, they're implemented that way, actually
    //condition! declares a module with the name of the condition, the module contains
    //  a single static value called cond, of type std::condition::Condition.  the cond
    //  value is the rendevous point between the site of error and where the error's handled
    //  It has two methods of interest, raise and trap
    //-raise maps a value of the condition's input type to it's output type
    //  the input type shoudl convey all relevant info to the handler, and the output type
    //  should convey all info needed to continue from site of the error
    //  if there's no handler, the condition will fail the task with appropriate error message
    use std::io::buffered::BufferedReader;
    use std::io::File;
    
    fn parse_int(x: &str) -> int {
        match from_str::<int>(x){
            Some(v) => v,
            None => malformed_int::cond.raise(x.to_owned())
        }
    }
    fn read_int_pairs_cond1(path: &str) -> ~[(int, int)]{
        let mut pairs = ~[];
        let path = Path::new(path);
        let mut reader = BufferedReader::new(File::open(&path));
        for line in reader.lines(){
            match line.words().to_owned_vec() {
                //delegate parsing to helper that will handle parse errors
                [a, b] => pairs.push((parse_int(a), parse_int(b))),
                //Raise the failed line to the handler
                //  If the condition returns things like an option or an Enum, 
                //    you can easily pattern match on it to handle the error
                _ => pairs.push(malformed_line::cond.raise(line.clone()))
            }
        }
        pairs
    }
    
    //Without trapping, the condition will fail!, with the error message
    fn trap_the_cond(path: &str){
        //Note, to handle multiple conditions, you have to nest the calls to cond.trap
        //handle malformed_int and return -1
        malformed_int::cond.trap(|_| -1).inside(|| {
            malformed_line::cond.trap(|_| (-1, -1)).inside(|| {
                //the protected logic
                let pairs = read_int_pairs_cond1(path);
                for &(a, b) in pairs.iter(){
                    println!("{:4.4d}, {:4.4d}", a, b);
                }
            })
        })
    }
}
*/

//Error handling use cases:
//Option/Result:
// -Frequent, expected error, and dealt with immediate caller
//Condition:
// -Can be handled at the site by one of a few strategies, and it's not clear
//  which strategy a caller would want
//Fail
// -Can't be handled at the site, and the only reasonable response is to
//  stop where you are


fn main() {
    //{} will print 'default format' of a type
    println!("{} is {}", "the answer", 42);
    //{:?} will conveniently print any type
    //println!("what is this thing?: {:?}", mystery_object);
    println("Hello?");
    
    //Example of spawning tasks
    //This string is immutable, so can be safely accessed
    // from multiple tasks
    let message = "This is from a lightweight thread";
    //For loops work with anything that implements the Iterator trait
    for num in range(0, 10){
        spawn(proc() {
            println(message);
            //println! is a macro that statically verifies a format string
            //Macros are structural, not textual
            println!("Message printed by task {:i}.", num)
        });
    }
    generic_list_test();
}
