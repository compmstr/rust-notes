#[feature(macro_rules)];

use std::libc::{c_int};
use std::io::{print, println};

pub static esc: &'static str = "\x1B[";

#[allow(dead_code)]
mod terminal_control {
  use std::libc::{c_int, c_uint, c_uchar};
  
  // Linux specifc termios structure definition
  //
  // Since we don't actually access any of the fields individually, and instead just
  // pass around termios as a "black box", this will probably work for other platforms
  // as long their struct termios is smaller than Linux's. For example, Mac OS omits the
  // c_line field and only has 20 control characters.
  #[allow(non_camel_case_types)]
  struct termios {
    c_iflag: c_uint, // input mode flags
    c_oflag: c_uint, // output mode flags
    c_cflag: c_uint, // control mode flags
    c_lflag: c_uint, // local mode flags
    c_line: c_uchar, // line discipline
    c_cc: [c_uchar, ..32], // control characters
    c_ispeed: c_uint, // input speed
    c_ospeed: c_uint, // output speed
  }

  extern {
    fn tcgetattr(filedes: c_int, termptr: *mut termios) -> c_int;
    fn tcsetattr(filedes: c_int, opt: c_int, termptr: *termios) -> c_int;
    fn cfmakeraw(termptr: *mut termios);
  }

  fn get_terminal_attr() -> (termios, c_int) {
    unsafe {
      let mut ios = termios {
        c_iflag: 0,
        c_oflag: 0,
        c_cflag: 0,
        c_lflag: 0,
        c_line: 0,
        c_cc: [0, ..32],
        c_ispeed: 0,
        c_ospeed: 0
      };
      // first parameter is file descriptor number, 0 ==> standard input
      let err = tcgetattr(0, &mut ios);
      return (ios, err);
    }
  }

  fn make_raw(ios: &termios) -> termios {
    unsafe {
      let mut ios = *ios;
      cfmakeraw(&mut ios);
      return ios;
    }
  }

  fn set_terminal_attr(ios: &termios) -> c_int {
    unsafe {
      // first paramter is file descriptor number, 0 ==> standard input
      // second paramter is when to set, 0 ==> now
      return tcsetattr(0, 0, ios);
    }
  }

  pub struct TerminalRestorer {
    ios: termios
  }

  impl Drop for TerminalRestorer {
    fn drop(&mut self) {
      set_terminal_attr(&self.ios);
    }
  }

  pub fn set_terminal_raw_mode() -> TerminalRestorer {
    let (original_ios, err) = get_terminal_attr();
    if err != 0 {
      fail!("failed to get terminal settings");
    }
    
    let raw_ios = make_raw(&original_ios);
    let err = set_terminal_attr(&raw_ios);
    if err != 0 {
      fail!("failed to switch terminal to raw mode");
    }
    
    TerminalRestorer {
      ios: original_ios
    }
  }
}

#[allow(dead_code)]
mod input_reader {
  use std::libc::{c_int, c_short, c_long};
  use std::cast::transmute;
  
  pub enum PollResult {
    PollReady,
    PollTimeout,
  }
  
  pub enum ReadResult {
    Up, Down, Right, Left, Other
  }
  
  #[allow(non_camel_case_types)]
  struct pollfd {
    fd: c_int,
    events: c_short,
    revents: c_short
  }

  extern {
    fn poll(fds: *mut pollfd, nfds: c_long, timeout: c_int) -> c_int;
    fn read(fd: c_int, buf: *mut u8, nbyte: u64) -> i64;
  }

  pub fn poll_stdin(timeoutMillis: c_int) -> PollResult {
    unsafe {
      let mut pfd = pollfd {
        fd: 0, // standard input file descriptor number
        events: 1, // POLLIN event
        revents: 0 // kernel modifies this field when calling poll()
      };
      let pr = poll(&mut pfd, 1, timeoutMillis);
      if pr > 0 {
        return PollReady
      } else if pr == 0 {
        return PollTimeout;
      } else {
        fail!("error polling standard input");
      }
    }
  }
  
  pub fn read_stdin() -> ReadResult {
    unsafe {
      // Reading bytes into storage for an unsigned integer for easy comparison of
      // input byte sequence (we only care about arrow keys) to integer constants
      //
      // At least for Konsole, pressing Up, Down, Right, or Left on the keyboard sends 3 bytes:
      // 0x1B (escape)
      // 0x5B [
      // 0x41, 0x42, 0x43, or 0x44 (A, B, C, or D)
      //
      // Note the case where we read less than all 3 bytes from the single read call is not handled,
      // and considered "Other"
      //
      // For example, 0x1B 0x5B, 0x44 is sent when Left is pressed
      //
      // The integer constants to compare these sequences to are "backwards" due to Intel's least significant
      // byte order, so 0x445B1B is the constant we expect when left is pressed
      
      let mut buf = 0u64;
      let bufAddr: *mut u8 = transmute(&mut buf);
      
      // first parameter is file descriptor number, 0 ==> standard input
      let numRead = read(0, bufAddr, 8);
      if numRead < 0 {
        fail!("error reading standard input");
      }
      match buf {
        0x415B1B => Up,
        0x425B1B => Down,
        0x435B1B => Right,
        0x445B1B => Left,
        _ => Other
      }
    }
  }
}

#[link(name="ncursesw")]
extern{
    fn raw() -> c_int;
    fn noraw() -> c_int;
    fn getch() -> c_int;
}

fn do_ansi(command: &str){
    let buf = ~esc.clone().to_owned();
    let buf = buf.append(command);
    print(buf);
}

fn move_cursor(row: int, col: int){
    let mut buf = ~"";
    buf.push_str(row.to_str());
    buf.push_char(';');
    buf.push_str(col.to_str());
    buf.push_char('H');
    do_ansi(buf);
}

fn clear(){do_ansi("2J");}
fn save_cursor(){do_ansi("s");}
fn restore_cursor(){do_ansi("u");}
fn hide_cursor(){do_ansi("?25l");}
fn show_cursor(){do_ansi("?25h");}

macro_rules! saving_location(
    ($body:expr) => (
        save_cursor();
        $body
        restore_cursor();
    );
)
macro_rules! at_xy(
    ($x:expr, $y:expr, $body:expr) => ({
        save_cursor();
        move_cursor($y, $x);
        $body
        restore_cursor();
    });
)

enum Color {
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White
}
enum Attr{
    Normal = 0,
    Bold,
    Blink = 5,
    Reverse = 7,
    Concealed
}
fn attr(att: Attr) -> int {att as int}
fn fg_color(col: Color) -> int {col as int + 30}
fn bg_color(col: Color) -> int {col as int + 40}

fn attr_str(elts: &[int]) -> ~str {
    let strs: ~[~str] = elts.iter().map(|x| x.to_str()).collect();
    let str = strs.connect(";");
    str
}
fn set_attrs(elts: &[int]){
    let mut atts = attr_str(elts);
    atts.push_char('m');
    do_ansi(atts);
}

fn main(){
    use std::io;
    //this raw() call doesn't seem to work
    unsafe{raw();}
    clear();
    move_cursor(10, 10);
    print("foobar");
    set_attrs([attr(Bold), fg_color(Green), bg_color(Blue)]);
    move_cursor(11, 10);
    print("baz");
    at_xy!(5, 5, {println("haha");});
    println(" more?");
    let dont_care = io::stdin().read_byte();
    unsafe{noraw();}
}
