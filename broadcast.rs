use std::vec::Vec;
use std::io::println;

pub struct Broadcaster<T>{
    listeners: ~Vec<Sender<T>>
}
impl<T: Send + Clone> Broadcaster<T>{
    fn new() -> Broadcaster<T>{
        Broadcaster{listeners: ~Vec::new()}
    }
    
    fn listen(&mut self) -> ~Receiver<T>{
        let (tx, rx) = channel();
        self.listeners.push(tx);
        ~rx
    }
    
    fn send(&mut self, msg: &T){
        //TODO: should be able to use filter to clean this up instead
        //  of the whole to_remove vector thing
        //This gets close, I think
        //*self.listeners = self.listeners.iter().filter(|tx| tx.try_send(msg.clone())).collect();
        let mut to_remove: Vec<uint> = Vec::new();
        for (idx, tx) in std::iter::count(0, 1).zip(self.listeners.iter()){
            match tx.try_send(msg.clone()) {
                true => {},
                false => to_remove.push(idx as uint)
            }
        }
        to_remove.reverse();
        for idx in to_remove.iter(){
            self.listeners.remove(*idx);
        }
    }
}

#[deriving(Clone)]
enum Message{
    Data(~str),
    MoveLeft,
    MoveRight,
    ExitIdx(int),
    Exit
}

fn main(){
    let mut bcast: Broadcaster<Message> = Broadcaster::new();
    
    for listen_id in range(0, 3){
        let listenData = bcast.listen();
        spawn(proc(){
            let rx = listenData;
            'main : loop{
                //Could do try_recv, which can be one of:
                //  Empty, Disconnected, or Data(T)
                match rx.recv(){
                    Data(text) => println("Got: \"" + text + "\" in " + listen_id.to_str()),
                    MoveLeft => println("Moving left in " + listen_id.to_str()),
                    MoveRight => println("Moving right in " + listen_id.to_str()),
                    ExitIdx(idx) if idx == listen_id => {
                        println("Exiting task at index: " + idx.to_str());
                        break 'main;
                    },
                    Exit => {println("Exiting task in " + listen_id.to_str()); break 'main},
                    ExitIdx(_) => {}
                }
            }
        });
    }

    bcast.send(&MoveLeft);
    bcast.send(&MoveRight);
    bcast.send(&Data(~"foobar"));
    bcast.send(&ExitIdx(1));
    bcast.send(&MoveLeft);
    bcast.send(&MoveRight);
    let mut buf = [0, ..128];
    std::io::stdin().read(buf);
    bcast.send(&MoveLeft);
    bcast.send(&MoveRight);
    bcast.send(&Exit);
}
