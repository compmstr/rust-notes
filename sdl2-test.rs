extern crate sdl2;
extern crate rand;
extern crate collections;
extern crate sync;
use rand::Rng;
use std::num::sqrt;
use std::vec_ng::Vec;
use sync::RWArc;

#[deriving(Clone)]
struct Vec2{x: f32, y: f32}
fn Vec2(x: f32, y: f32) -> Vec2 {
    Vec2 {x: x, y: y}
}

#[allow(dead_code)]
impl Vec2{
    fn square(size: f32) -> Vec2 {
        Vec2(size, size)
    }
    fn dot(&self, other: Vec2) -> f32{
        self.x * other.x + self.y * other.y
    }
    fn dist(&self, other: Vec2) -> f32{
        sqrt(self.dot(other))
    }
    fn mag(&self) -> f32 {
        sqrt(self.dot(*self))
    }
    fn mag_squared(&self) -> f32 {
        self.dot(*self)
    }
    fn norm(&self) -> Vec2 {
        let mag_inv = 1.0 / self.mag();
        self * mag_inv
    }
}
impl Add<Vec2, Vec2> for Vec2{
    fn add(&self, rhs: &Vec2) -> Vec2{
        Vec2(self.x + rhs.x, self.y + rhs.y)
    }
}
impl Sub<Vec2, Vec2> for Vec2{
    fn sub(&self, rhs: &Vec2) -> Vec2{
        Vec2(self.x - rhs.x, self.y - rhs.y)
    }
}
impl Mul<f32, Vec2> for Vec2{
    fn mul(&self, rhs: &f32) -> Vec2{
        Vec2(self.x * *rhs, self.y * *rhs)
    }
}


trait Drawable{
    fn draw(&self, &sdl2::render::Renderer);
}
fn draw_rect(pos: Vec2, size: Vec2, color: sdl2::pixels::Color, rend: &sdl2::render::Renderer){
    let rec = sdl2::rect::Rect(pos.x as i32, pos.y as i32, size.x as i32, size.y as i32);
    rend.set_draw_color(color);
    rend.fill_rect(&rec);
}

trait Logic{
    fn update(&mut self, f32);
}

struct Player{
    entity: DynEntity,
    input: RWArc<input::Input>
}
impl Player{
    fn new(loc: Vec2, input: RWArc<input::Input>) -> Player{
        Player{entity: DynEntity::new(loc, Vec2::square(15.0)),
               input: input}
    }
}
impl Drawable for Player{
    fn draw(&self, rend: &sdl2::render::Renderer){
        draw_rect(self.entity.pos, self.entity.size, sdl2::pixels::RGB(0, 255, 0), rend);
    }
}

impl Logic for Player{
    fn update(&mut self, d_time: f32){
        let input = self.input.clone();
        input.read(|input|{
            if input.is_action_released(input::MoveLeft) { self.entity.vel.x = 0.0; }
            if input.is_action_released(input::MoveRight) { self.entity.vel.x = 0.0; }
            if input.is_action_released(input::MoveUp) { self.entity.vel.y = 0.0; }
            if input.is_action_released(input::MoveDown) { self.entity.vel.y = 0.0; }

            if input.is_action_held(input::MoveLeft) { self.entity.vel.x = -100.0; }
            if input.is_action_held(input::MoveRight) { self.entity.vel.x = 100.0; }
            if input.is_action_held(input::MoveUp) { self.entity.vel.y = -100.0; }
            if input.is_action_held(input::MoveDown) { self.entity.vel.y = 100.0; }
        });
    }
}

struct Enemy{
    entity: DynEntity
}
impl Logic for Enemy{
    fn update(&mut self, d_time: f32){
    }
}
impl Drawable for Enemy{
    fn draw(&self, rend: &sdl2::render::Renderer){
        draw_rect(self.entity.pos, self.entity.size, sdl2::pixels::RGB(0, 0, 255), rend);
    }
}

static mut NEXT_ENTITY_ID: uint = 0;
struct DynEntity{
    id: uint,
    pos: Vec2,
    size: Vec2,
    vel: Vec2
}

impl DynEntity{
    fn new(pos: Vec2, size: Vec2) -> DynEntity{
        let id = unsafe{NEXT_ENTITY_ID += 1; NEXT_ENTITY_ID - 1};
        DynEntity{id: id, pos: pos, size: size, vel: Vec2(0.0, 0.0)}
    }
    fn update(&mut self, d_time: f32){
        self.pos = self.pos + (self.vel * d_time);
    }
}

fn clear_rend(rend: &sdl2::render::Renderer) {
    rend.set_draw_color(sdl2::pixels::RGB(255, 0, 255));
    rend.clear();
}

struct World{
    player: Option<Player>,
    enemies: Vec<Enemy>,
}
struct Game{
    world: World,
    exiting: bool
}
fn Game() -> Game {
    Game{world: World{player: None, enemies: Vec::new()}, exiting: false}
}
impl Game{
    fn make_player(input: RWArc<input::Input>) -> Player{
        Player::new(Vec2(10.0, 10.0), input)
    }
    fn make_enemy(loc: Vec2) -> Enemy{
        let mut new = Enemy{entity: DynEntity::new(loc, Vec2::square(10.0))};
        new.entity.vel.x = 3.0;
        new.entity.vel.y = 3.0;
        new
    }
    fn init(&mut self, input: RWArc<input::Input>){
        self.world.player = Some(Game::make_player(input));
        self.world.enemies.push(Game::make_enemy(Vec2(100.0, 100.0)));
        self.world.enemies.push(Game::make_enemy(Vec2(200.0, 100.0)));
        self.world.enemies.push(Game::make_enemy(Vec2(100.0, 200.0)));
    }
    fn draw(&self, rend: &sdl2::render::Renderer){
        match &self.world.player{
            &Some(ref player) => player.draw(rend),
            &None => {}
        }
        for enemy in self.world.enemies.iter(){
            enemy.draw(rend);
        }
    }
    fn update(&mut self, input: RWArc<input::Input>, d_time: f32){
        for player in self.world.player.mut_iter() { 
            player.update(d_time);
            player.entity.update(d_time);
        }
        for enemy in self.world.enemies.mut_iter(){
            enemy.update(d_time);
            enemy.entity.update(d_time);
        }
        input.read(|input|{
            if input.is_action_pressed(input::Exit){
                self.exit();
            }
        });
    }

    fn exit(&mut self){ self.exiting = true; }
}

mod input{
    extern crate sdl2;
    use collections::hashmap::HashMap;

    #[deriving(Hash, Eq)]
    pub enum KeyAction{
        MoveLeft,
        MoveRight,
        MoveUp,
        MoveDown,
        Exit
    }
    pub struct Input{
        key_map: HashMap<sdl2::keycode::KeyCode, KeyAction>,
        pressed_actions: HashMap<KeyAction, bool>,
        released_actions: HashMap<KeyAction, bool>,
        held_actions: HashMap<KeyAction, bool>
    }
    impl Input {
        pub fn new() -> Input {
            let mut ret = Input{
                key_map: HashMap::new(),
                pressed_actions: HashMap::new(),
                released_actions: HashMap::new(),
                held_actions: HashMap::new()
            };
            
            ret.insert_key_action(MoveRight, sdl2::keycode::DKey);
            ret.insert_key_action(MoveLeft, sdl2::keycode::AKey);
            ret.insert_key_action(MoveUp, sdl2::keycode::WKey);
            ret.insert_key_action(MoveDown, sdl2::keycode::SKey);
            ret.insert_key_action(Exit, sdl2::keycode::EscapeKey);
            
            ret
        }
        
        pub fn on_key_down(&mut self, keycode: sdl2::keycode::KeyCode){
            match self.key_map.find(&keycode){
                Some(action) => {
                    self.pressed_actions.insert(*action, true);
                    self.held_actions.insert(*action, true);
                },
                None => {}
            }
        }
        pub fn on_key_up(&mut self, keycode: sdl2::keycode::KeyCode){
            match self.key_map.find(&keycode){
                Some(action) => {
                    self.released_actions.insert(*action, true);
                    self.held_actions.insert(*action, false);
                },
                None => {}
            }
        }

        pub fn insert_key_action(&mut self, action: KeyAction, keycode: sdl2::keycode::KeyCode){
            self.key_map.insert(keycode, action);
        }
        
        pub fn is_action_pressed(&self, action: KeyAction) -> bool{
            match self.pressed_actions.find(&action){
                Some(cur) => *cur,
                None => false
            }
        }
        pub fn is_action_held(&self, action: KeyAction) -> bool{
            match self.held_actions.find(&action){
                Some(cur) => *cur,
                None => false
            }
        }
        pub fn is_action_released(&self, action: KeyAction) -> bool{
            match self.released_actions.find(&action){
                Some(cur) => *cur,
                None => false
            }
        }
        pub fn new_cycle(&mut self){
            self.pressed_actions.clear();
            self.released_actions.clear();
        }
    }
}

#[main]
pub fn main(){
    
    sdl2::init([sdl2::InitVideo]);

    let window = match sdl2::video::Window::new("rust-sdl2 demo", sdl2::video::PosCentered,
                                                sdl2::video::PosCentered, 800, 600,
                                                [sdl2::video::OpenGL]){
        Ok(window) => window,
        Err(err) => fail!(format!("failed to create window: {}", err))
    };
    let rend = match sdl2::render::Renderer::from_window(window, sdl2::render::DriverAuto,
                                                         [sdl2::render::Accelerated]){
        Ok(renderer) => renderer,
        Err(err) => fail!(format!("failed to create renderer: {}", err))
    };

    let mut game = Game();
    let input = RWArc::new(input::Input::new());
    game.init(input.clone());
    
    let mut last_frame = sdl2::get_ticks();
    let mut d_time: f32;
    'main : loop{
        input.write(|input| input.new_cycle());
        'event : loop{
            match sdl2::event::poll_event(){
                sdl2::event::QuitEvent(_) => break 'main,
                sdl2::event::NoEvent => break 'event,
                sdl2::event::KeyDownEvent(_, _, k, _, _) => input.write(|input| input.on_key_down(k)),
                sdl2::event::KeyUpEvent(_, _, k, _, _) => input.write(|input| input.on_key_up(k)),
                _ => {}
            };
        }
        if game.exiting {
            break 'main
        }
        let new_time = sdl2::get_ticks();
        d_time = ((new_time - last_frame) as f32) / 1000.0;
        last_frame = new_time;

        game.update(input.clone(), d_time);
        clear_rend(rend);
        game.draw(rend);
        rend.present();
    }
    sdl2::quit();
}
