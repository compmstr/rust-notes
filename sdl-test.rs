extern crate sdl;
extern crate rand;
extern crate collections;
use rand::Rng;
use std::num::sqrt;
use std::vec_ng::Vec;

use collections::hashmap::HashMap;

#[deriving(Clone)]
struct Vec2{x: f32, y: f32}
fn Vec2(x: f32, y: f32) -> Vec2 {
    Vec2 {x: x, y: y}
}

#[allow(dead_code)]
impl Vec2{
    fn add(&self, other: Vec2) -> Vec2{
        Vec2(self.x + other.x, self.y + other.y)
    }
    fn sub(&self, other: Vec2) -> Vec2{
        Vec2(self.x - other.x, self.y - other.y)
    }
    fn scale(&self, amt: f32) -> Vec2 {
        Vec2(self.x * amt, self.y * amt)
    }
    fn dot(&self, other: Vec2) -> f32{
        self.x * other.x + self.y * other.y
    }
    fn dist(&self, other: Vec2) -> f32{
        sqrt(self.dot(other))
    }
    fn mag(&self) -> f32 {
        sqrt(self.dot(*self))
    }
    fn mag_squared(&self) -> f32 {
        self.dot(*self)
    }
    fn norm(&self) -> Vec2 {
        let mag_inv = 1.0 / self.mag();
        self.scale(mag_inv)
    }
}

struct Drawable{
    color: sdl::video::Color,
    size: Vec2
}
fn Drawable(color: sdl::video::Color, size: Vec2) -> Drawable{
    Drawable{color: color, size: size}
}
impl Drawable{
    fn draw(&self, pos: Vec2, screen: &sdl::video::Surface){
        let rec = sdl::Rect(pos.x as i16, pos.y as i16, self.size.x as u16, self.size.y as u16);
        screen.fill_rect(Some(rec), self.color);
    }
}

struct DynEntity{
    pos: Vec2,
    vel: Vec2,
    drawer: Drawable
}
fn DynEntity(pos: Vec2, color: sdl::video::Color, size: f32) -> DynEntity{
    DynEntity{pos: pos, vel: Vec2(0.0, 0.0), drawer: Drawable(color, Vec2(size, size))}
}
impl DynEntity{
    fn draw(&self, screen: &sdl::video::Surface){
        self.drawer.draw(self.pos, screen);
    }
    fn update(&mut self, d_time: f32){
        self.pos = self.pos.add(self.vel.scale(d_time));
    }
    
}

fn clear_screen(screen: &sdl::video::Surface) {
    screen.fill_rect(None, sdl::video::RGB(255, 0, 255));
}

struct Game{
    player: Option<DynEntity>,
    enemies: Vec<DynEntity>,
    exiting: bool
}
fn Game() -> Game {
    Game{player: None, enemies: Vec::new(), exiting: false}
}
impl Game{
    fn make_player() -> DynEntity{
        DynEntity(Vec2(10.0, 10.0), sdl::video::RGB(0, 255, 0), 15.0)
    }
    fn make_enemy(loc: Vec2) -> DynEntity{
        let mut new = DynEntity(loc, sdl::video::RGB(0, 0, 255), 10.0);
        new.vel.x = 3.0;
        new.vel.y = 3.0;
        new
    }
    fn init(&mut self){
        self.player = Some(Game::make_player());
        self.enemies.push(Game::make_enemy(Vec2(100.0, 100.0)));
        self.enemies.push(Game::make_enemy(Vec2(200.0, 100.0)));
        self.enemies.push(Game::make_enemy(Vec2(100.0, 200.0)));
    }
    fn draw(&self, screen: &sdl::video::Surface){
        self.player.map(|player| player.draw(screen));
        for enemy in self.enemies.iter(){
            enemy.draw(screen);
        }
    }
    fn update(&mut self, d_time: f32){
        self.doto_player(|player| player.update(d_time));
        for enemy in self.enemies.mut_iter(){
            enemy.update(d_time);
        }
    }

    fn doto_player(&mut self, f: |&mut DynEntity|){
        for player in self.player.mut_iter() {
            f(player);
        }
    }
}

mod actions{
    use super::Game;
    
    pub fn move_left(game: &mut Game){ game.doto_player(|player| {player.vel.x += -100.0}); }
    pub fn move_right(game: &mut Game){ game.doto_player(|player| {player.vel.x += 100.0});}
    pub fn move_up(game: &mut Game){ game.doto_player(|player| {player.vel.y += -100.0});}
    pub fn move_down(game: &mut Game){ game.doto_player(|player| {player.vel.y += 100.0});}
    pub fn exit_game(game: &mut Game){ game.exiting = true; }
}

mod input{
    extern crate sdl;
    use actions;
    use collections::hashmap::HashMap;
    use super::Game;
    #[deriving(Hash, Eq)]
    pub enum KeyAction{
        MoveLeft,
        MoveRight,
        MoveUp,
        MoveDown,
        Exit
    }

    pub fn init(key_map: &mut HashMap<uint, KeyAction>,
                key_down_procs: &mut HashMap<KeyAction, fn(&mut Game) -> ()>,
                key_up_procs: &mut HashMap<KeyAction, fn(&mut Game) -> ()>){ 
        /*key_map.insert(sdl::event::RightKey as uint, MoveRight);
        key_map.insert(sdl::event::LeftKey as uint, MoveLeft);
        key_map.insert(sdl::event::UpKey as uint, MoveUp);
        key_map.insert(sdl::event::DownKey as uint, MoveDown);*/
        key_map.insert(sdl::event::DKey as uint, MoveRight);
        key_map.insert(sdl::event::AKey as uint, MoveLeft);
        key_map.insert(sdl::event::WKey as uint, MoveUp);
        key_map.insert(sdl::event::SKey as uint, MoveDown);
        key_map.insert(sdl::event::EscapeKey as uint, Exit);

        key_down_procs.insert(MoveRight, actions::move_right);
        key_down_procs.insert(MoveLeft, actions::move_left);
        key_down_procs.insert(MoveUp, actions::move_up);
        key_down_procs.insert(MoveDown, actions::move_down);
        key_down_procs.insert(Exit, actions::exit_game);
        key_up_procs.insert(MoveRight, actions::move_left);
        key_up_procs.insert(MoveLeft, actions::move_right);
        key_up_procs.insert(MoveUp, actions::move_down);
        key_up_procs.insert(MoveDown, actions::move_up);
        key_up_procs.insert(Exit, actions::exit_game);
    }
}

#[main]
pub fn main(){
    
    sdl::init([sdl::InitVideo]);
    sdl::wm::set_caption("Rust sdl test", "rust-sdl");
    let mut key_map: HashMap<uint, input::KeyAction> = HashMap::new();
    let mut key_down_procs: HashMap<input::KeyAction, fn(&mut Game) -> ()> = HashMap::new();
    let mut key_up_procs: HashMap<input::KeyAction, fn(&mut Game) -> ()> = HashMap::new();
    input::init(&mut key_map, &mut key_down_procs, &mut key_up_procs);
    
    let screen = match sdl::video::set_video_mode(800, 600, 32, [sdl::video::HWSurface],
                                                                [sdl::video::DoubleBuf]){
        Ok(screen) => screen,
        Err(err) => fail!("Failed to set video mode: {}", err)
    };
    let mut game = Game();
    game.init();
    
    let mut last_frame = sdl::get_ticks();
    let mut d_time: f32;
    'main : loop{
        'event : loop{
            match sdl::event::poll_event(){
                sdl::event::QuitEvent => break 'main,
                sdl::event::NoEvent => break 'event,
                sdl::event::KeyEvent(k, true, _, _) =>
                    match key_map.find(&(k as uint)){
                        Some(command) =>
                            match key_down_procs.find(command){
                                Some(f) => (*f)(&mut game),
                                None => {}
                            },
                        None => {}
                    },
                sdl::event::KeyEvent(k, false, _, _) =>
                    match key_map.find(&(k as uint)){
                        Some(command) =>
                            match key_up_procs.find(command){
                                Some(f) => (*f)(&mut game),
                                None => {}
                            },
                        None => {}
                    },
                _ => {}
            };
        }
        if game.exiting {
            break 'main
        }
        let new_time = sdl::get_ticks();
        d_time = ((new_time - last_frame) as f32) / 1000.0;
        last_frame = new_time;

        game.update(d_time);
        clear_screen(screen);
        game.draw(screen);
        screen.flip();
    }
    sdl::quit();
}
