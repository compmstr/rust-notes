use std::f64;

static TILE_SIZE: i32 = 32;
static SCALE: f64 = 1.0; // Divider of TILE_SIZE, 1.0 -> 32, 2.0 -> 16, etc

pub trait AsGameCoord { fn to_game_coord(&self) -> GameCoord; }
pub trait AsPixelCoord { fn to_pixel_coord(&self) -> PixelCoord; }
pub trait AsTileCoord { fn to_tile_coord(&self) -> TileCoord; }

/// GameCoord - density-independent distance in pixels.
///   Converting a GameCoord to a PixelCoord will round to nearest coord,
///   scaled based on desired tile size and resolution
#[deriving(Eq,Ord)]
pub struct GameCoord(f64);

impl AsGameCoord for GameCoord{
    #[inline(always)]
    fn to_game_coord(&self) -> GameCoord { *self }
}
impl AsPixelCoord for GameCoord{
    #[inline(always)]
    fn to_pixel_coord(&self) -> PixelCoord {
        let GameCoord(a) = *self;
        PixelCoord(f64::round(a / SCALE) as i32)
    }
}
impl AsTileCoord for GameCoord{
    #[inline(always)]
    fn to_tile_coord(&self) -> TileCoord {
        let GameCoord(a) = *self;
        TileCoord((a / TILE_SIZE as f64) as uint)
    }
}

//Allow '+' to work on anything that can be converted to_game
impl <T: AsGameCoord> Add<T, GameCoord> for GameCoord {
    #[inline(always)]
    fn add(&self, rhs: &T) -> GameCoord {
        let GameCoord(a) = *self;
        let GameCoord(b) = rhs.to_game_coord();
        GameCoord(a + b)
    }
}
//Allow '-' to work on anything that can be converted to_game
impl <T: AsGameCoord> Sub<T, GameCoord> for GameCoord {
    #[inline(always)]
    fn sub(&self, rhs: &T) -> GameCoord {
        let GameCoord(a) = *self;
        let GameCoord(b) = rhs.to_game_coord();
        GameCoord(a - b)
    }
}

//Allow '*' to work on anything that can be converted to_game
impl <T: AsGameCoord> Mul<T, GameCoord> for GameCoord {
    #[inline(always)]
    fn mul(&self, rhs: &T) -> GameCoord {
        let GameCoord(a) = *self;
        let GameCoord(b) = rhs.to_game_coord();
        GameCoord(a * b)
    }
}

//Allow '/' to work on anything that can be converted to_game
impl <T: AsGameCoord> Div<T, GameCoord> for GameCoord {
    #[inline(always)]
    fn div(&self, rhs: &T) -> GameCoord {
        let GameCoord(a) = *self;
        let GameCoord(b) = rhs.to_game_coord();
        GameCoord(a / b)
    }
}

//Pixel coordinate on the screen
#[deriving(Eq,Ord)]
pub struct PixelCoord(i32);

impl AsPixelCoord for PixelCoord{
    #[inline(always)]
    fn to_pixel_coord(&self) -> PixelCoord { *self }
}
impl <T: AsPixelCoord> Add<T, PixelCoord> for PixelCoord{
    #[inline(always)]
    fn add(&self, rhs: &T) -> PixelCoord {
        let PixelCoord(a) = *self;
        let PixelCoord(b) = rhs.to_pixel_coord();
        PixelCoord(a + b)
    }
}

/// Tile is the tile coordinate in the game's ase tile size (32 pixels)
/// may be scaled when converted to GameCoords or PixelCoords
#[deriving(Eq,Ord)]
pub struct TileCoord(uint);

impl AsGameCoord for TileCoord{
    #[inline(always)]
    fn to_game_coord(&self) -> GameCoord {
        let TileCoord(a) = *self;
        GameCoord((a * (TILE_SIZE as uint)) as f64)
    }
}
impl AsPixelCoord for TileCoord{
    #[inline(always)]
    fn to_pixel_coord(&self) -> PixelCoord {
        self.to_game_coord().to_pixel_coord()
    }
}
impl AsTileCoord for TileCoord{
    #[inline(always)]
    fn to_tile_coord(&self) -> TileCoord { *self }
}

//Allow '+' to work on anything that can be converted to_tile
impl <T: AsTileCoord> Add<T, TileCoord> for TileCoord {
    #[inline(always)]
    fn add(&self, rhs: &T) -> TileCoord {
        let TileCoord(a) = *self;
        let TileCoord(b) = rhs.to_tile_coord();
        TileCoord(a + b)
    }
}
//Allow '-' to work on anything that can be converted to_tile
impl <T: AsTileCoord> Sub<T, TileCoord> for TileCoord {
    #[inline(always)]
    fn sub(&self, rhs: &T) -> TileCoord {
        let TileCoord(a) = *self;
        let TileCoord(b) = rhs.to_tile_coord();
        TileCoord(a - b)
    }
}

//Allow '*' to work on anything that can be converted to_tile
impl <T: AsTileCoord> Mul<T, TileCoord> for TileCoord {
    #[inline(always)]
    fn mul(&self, rhs: &T) -> TileCoord {
        let TileCoord(a) = *self;
        let TileCoord(b) = rhs.to_tile_coord();
        TileCoord(a * b)
    }
}

//Allow '/' to work on anything that can be converted to_tile
impl <T: AsTileCoord> Div<T, TileCoord> for TileCoord {
    #[inline(always)]
    fn div(&self, rhs: &T) -> TileCoord {
        let TileCoord(a) = *self;
        let TileCoord(b) = rhs.to_tile_coord();
        TileCoord(a / b)
    }
}

type Millis = i64;
pub fn main(){
    let t: Millis = 25;
    let t2: i64 = 32;
    let t3 = t + t2;
}
